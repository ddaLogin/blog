@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row m-post-group">
            <h3><i class="fa fa-search fa-2x" aria-hidden="true"></i> Результаты поиска</h3>
            <hr>
            <div class="row">
                @forelse($posts as $post)
                    {{ Html::post($post) }}
                @empty
                    <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                @endforelse
            </div>
            <hr>
            {{$posts->render()}}
        </div>
    </div>
@endsection
