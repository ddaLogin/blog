<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="shortcut icon" href="/favicon.ico" />
    <title></title>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <link rel="stylesheet" href="/styles/style.css">
    <link rel="stylesheet" href="/styles/bootstrapDarkly.css">
</head>
<body>
<div class="container text-center">
    <br><br><br>
    <i class="fa fa-meh-o fa-5x" aria-hidden="true"></i>
    <br>
    <h1 class="text-warning">404 Указанный ресурс не найден</h1>
    <hr>
    <a href="/" class="btn btn-primary">Вернуться на главную</a>
</div>
</body>
</html>
