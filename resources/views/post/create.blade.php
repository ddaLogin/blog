@extends('layouts.app')

@section('head')
    <title>Pencil| публикация</title>
@endsection

@section('content')
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Выберите обложку</h4>
            </div>
            <div class="modal-body">
                <form name="formPhoto" method="post" enctype="multipart/form-data" class="hidden">
                    {{Form::file('album', ['id' => 'newAlbum', 'data-ajax' => 'upload', 'data-upload-link' => '/file/album', 'data-ajax-progress' => '#uploadProgress', 'data-ajax-complete' => 'updateAlbums'])}}
                </form>
                <div class="row" id="albumList">
                    @foreach(Auth::user()->albums as $photo)
                        @if(!empty(old('album_id')) && $photo->id == old('album_id'))
                            <?php $src =  $photo->photo; ?>
                        @endif
                        <div onclick='setPreview("{{$photo->id}}", "{{$photo->photo}}")' class="m-album col col-md-3" style="background-image: url({{$photo->photo}});" data-dismiss="modal" ></div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <div id="uploadProgress" class="progress hidden">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span class="sr-only">45% Complete</span>
                    </div>
                </div>
                <button class="btn btn-success" data-click="#newAlbum"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Загрузить новую</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    {!! Form::open(array('route' => 'post.store', 'class' => 'form-horizontal')) !!}
    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="category_id" class="col-md-1 control-label">Категория</label>
        <div class="col-md-10">
            {!! Form::select('category_id', $categories->pluck('name', 'id'), null, ['class' => 'form-control']) !!}
            @if ($errors->has('category_id'))
                <span class="help-block"> <strong>{{ $errors->first('category_id') }}</strong> </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="title" class="col-md-1 control-label">Заголовок</label>
        <div class="col-md-10">
            <input type="text" name="title" class="form-control" value="{{ old('title') }}">
            @if ($errors->has('title'))
                <span class="help-block"> <strong>{{ $errors->first('title') }}</strong> </span>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('album_id') ? ' has-error' : '' }}">
        <label for="title" class="col-md-1 control-label"></label>
        <div class="col-md-10">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Выбрать обложку
            </button>
            <img src="{{ $src or '/empty.png' }}" id="albumPreview" height="45" alt="" >
            <input type="hidden" name="album_id" id="albumId" value="{{ old('album_id') }}">
            @if ($errors->has('album_id'))
                <span class="help-block"> <strong>{{ $errors->first('album_id') }}</strong> </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
        <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
        <script type="text/javascript">
            bkLib.onDomLoaded(function () {
                nicEditors.allTextAreas()
            });
        </script>
        <label for="body" class="col-md-1 control-label">Текст</label>
        <div class="col-md-10">
            {{ Form::textarea('body',  old('body'), ['class' => 'form-control']) }}
            @if ($errors->has('body'))
                <span class="help-block"> <strong>{{ $errors->first('body') }}</strong> </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-11 text-right">
            <button type="submit" class="btn btn-success">
                Опубликовать
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
