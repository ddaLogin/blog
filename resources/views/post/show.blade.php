@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-md-3">
                <img src="{{$post->user->photo}}" height="150" alt="">
            </div>
            <div class="col col-md-9">
                <a href="{{route('user.show', ['id' => $post->user->id])}}"><h4>{{$post->user->name}}</h4></a>
                <h4>{{$post->user->email}}</h4>
                {{Html::subscription($post->user)}}
            </div>
        </div>
        <hr>
        @if (Auth::check() && Auth::user()->id == $post->user->id)
            <div class="btn-group pull-right">
                <a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
                {{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'delete', 'style' => 'display:inline;')) }}
                <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                {{ Form::close() }}
            </div>
        @endif
        <div class="col col-md-12">
            <div class="col col-md-5">
                <img src="{{$post->album->photo}}" class="img-responsive" alt="">
            </div>
            <h2>{{$post->title}}</h2>
            <div class="row">
                <p class="m-text-wrap">{!! $post->body !!}</p>
            </div>
            <hr>
            <h4>Комментарии <span class="label label-default">{{count($post->comments)}}</span></h4>
            {!! Form::open(array('route' => 'comment.store', 'class' => 'form-horizontal')) !!}
            {!! Form::hidden('post_id', $post->id) !!}
            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                <div class="col-md-12">
                    {{ Form::textarea('body', old('body'), ['class' => 'form-control', 'rows' => '5']) }}
                    @if ($errors->has('body'))
                        <span class="help-block"> <strong>{{ $errors->first('body') }}</strong> </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-success">
                        Отправить
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
            <hr>
            @foreach($post->comments as $comment)
                {{ Html::comment($comment) }}
            @endforeach
        </div>
    </div>
@endsection
