@extends('layouts.app')

@section('head')
    <title>Pencil</title>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h3><i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i> Новые публикации</h3>
            <hr>
            @forelse($news as $post)
                {{ Html::post($post) }}
            @empty
                <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
            @endforelse
        </div>
        <br>
        <div class="row m-post-group">
            <h3><i class="fa fa-comments fa-2x" aria-hidden="true"></i> Самые обсуждаемые в этом месяце</h3>
            <hr>
            @forelse($hots as $post)
                {{ Html::post($post) }}
            @empty
                <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
            @endforelse
        </div>
    </div>
@endsection
