<div class="media m-comment">
    <a class="pull-left">
        <img class="media-object" src="{{$comment->user->photo}}" height="60" alt="...">
    </a>
    @if (Auth::check() && Auth::user()->id == $comment->user->id)
        {{ Form::open(array('route' => array('comment.destroy', $comment->id), 'method' => 'delete', 'style' => 'display:inline;')) }}
            <button type="submit" class="btn btn-sm btn-default pull-right m-button-close" aria-hidden="true"><i class="fa fa-times"></i></button>
        {{ Form::close() }}
    @endif
    <div class="media-body">
        <a href="{{route('user.show', ['id' => $comment->user->id])}}" class="media-heading">{{$comment->user->name}}</a>
        <p class="m-text-wrap">{{$comment->body}}</p>
        <div class="col col-md-12 text-right">{{$comment->created_at}}</div>
    </div>
</div>
<hr>
