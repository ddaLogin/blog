@if (Auth::check() && Auth::user()->id != $user->id)
    @if (Auth()->user()->subscriptions->find($user->id))
        <a href="{{route('user.unsubscribe', ['id' => $user->id])}}" class="btn btn-danger btn-sm">Отписаться</a>
    @else
        <a href="{{route('user.subscribe', ['id' => $user->id])}}" class="btn btn-success btn-sm">Подписаться</a>
    @endif
@endif
