<div class="m-post-container col col-md-4" style="background-image: url({{$post->album->photo}});">
    <div class="m-post-author">
        <a class="pull-left m-text-white" href="{{route('user.show', ['id' => $post->user->id])}}">{{$post->user->name}}</a>
        <label for="" class="pull-right m-text-white">{{$post->category->name}}</label>
    </div>
    <div class="m-post-title text-left">
        <a class="m-text-white" href="{{route('post.show', ['id' => $post->id])}}">{{$post->title}}</a>
    </div>
    @if (Auth::check() && Auth::user()->id == $post->user->id)
        <div class="btn-group-vertical pull-right">
            <a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
            {{ Form::open(array('route' => array('post.destroy', $post->id), 'method' => 'delete', 'style' => 'display:inline;')) }}
            <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
            {{ Form::close() }}
        </div>
    @endif
</div>
