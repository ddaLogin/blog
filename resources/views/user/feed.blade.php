@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row m-post-group">
            <h3><i class="fa fa-th-list fa-2x" aria-hidden="true"></i> Лента</h3>
            <hr>
            <div class="row">
                @forelse($posts as $post)
                    {{ Html::post($post) }}
                @empty
                    <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                @endforelse
            </div>
            {{$posts->render()}}
        </div>
    </div>
@endsection
