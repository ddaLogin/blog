@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-md-3">
                <div class="m-main-avatar">
                    <img id="photo" src="{{$user->photo}}" style="width: 100%;" alt="">
                    @if (Auth::check() && Auth::user()->id == $user->id)
                        <form class="hidden" name="formAvatar" method="post" enctype="multipart/form-data">
                            {{Form::file('photo', ['id' => 'newPhoto', 'data-ajax' => 'upload', 'data-upload-link' => '/file/photo', 'data-ajax-progress' => '#uploadProgress', 'data-ajax-complete' => 'updateAvatar'])}}
                        </form>
                        <button class="btn btn-success btn-sm m-button-upload" data-click="#newPhoto"><i class="fa fa-camera" aria-hidden="true"></i></button>
                    @endif
                </div>
                <div id="uploadProgress" class="progress hidden">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                </div>
            </div>
            <div class="col col-md-9">
                <h4>{{$user->name}}</h4>
                <h4>{{$user->email}}</h4>
                {{Html::subscription($user)}}
            </div>
        </div>
        <hr>
        <div class="row">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#posts" data-toggle="tab">Публикации <span class="label label-primary">{{count($user->posts)}}</span></a></li>
                <li><a href="#subscribers" data-toggle="tab">Подписчики <span class="label label-primary">{{count($user->subscribers)}}</span></a></li>
                <li><a href="#subscription" data-toggle="tab">Подписки <span class="label label-primary">{{count($user->subscriptions)}}</span></a></li>
            </ul>
            <br>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="posts">
                    @forelse($user->posts as $post)
                        {{ Html::post($post) }}
                    @empty
                        <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                    @endforelse
                </div>
                <div class="tab-pane" id="subscribers">
                    @forelse($user->subscribers as $subscriber)
                        {{ Html::user($subscriber) }}
                    @empty
                        <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                    @endforelse
                </div>
                <div class="tab-pane" id="subscription">
                    @forelse($user->subscriptions as $subscription)
                        {{ Html::user($subscription) }}
                    @empty
                        <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
