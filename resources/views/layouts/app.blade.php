<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="shortcut icon" href="/favicon.ico" />

    @yield('head')

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"--}}
          {{--integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
    <link rel="stylesheet" href="/styles/style.css">
    <link rel="stylesheet" href="/styles/bootstrapDarkly.css">
</head>
<body id="app-layout">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a href="/" class="navbar-left"><img height="60" src="/logo.svg"></a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a class="m-nav-link" href="{{ route('home.new') }}"><i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i></a></li>
                <li><a class="m-nav-link" href="{{ route('home.hot') }}"><i class="fa fa-comments fa-2x" aria-hidden="true"></i></a></li>
                @if (!Auth::guest())
                    <li><a class="m-nav-link" href="{{ route('user.feed') }}"><i class="fa fa-th-list fa-2x" aria-hidden="true"></i></a></li>
                @endif
            </ul>
            <form class="navbar-form navbar-left" role="search" action="{{route('home.search')}}" method="get">
                <div class="input-group">
                    <input type="hidden" name="category_id" id="category_id" >
                    <input type="text" name="q" class="form-control" aria-label="..." placeholder="Поиск" {{ (isset($_GET["q"]) and !empty($_GET["q"])) ? "value=$_GET[q]" : '' }}>
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="categoryName">Все категории</span><span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            @foreach($categories as $category)
                                <li><a data-bind-mirror="#categoryName" data-bind-target="#category_id" data-bind-value="{{$category->id}}" >{{$category->name}}</a oncli></li>
                            @endforeach
                            <li role="separator" class="divider"></li>
                            <li><a data-bind-mirror="#categoryName" data-bind-target="#category_id" data-bind-value="">Все категории</a></li>
                        </ul>
                    </div><!-- /btn-group -->
                </div><!-- /input-group -->
                <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Войти</a></li>
                    <li><a href="{{ url('/register') }}">Регистрация</a></li>
                @else
                    <li class="dropdown">
                        {{--style="background-image: url({{Auth::user()->photo}});"--}}
                        <a  href="#"
                           class="dropdown-toggle m-user-bar" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('user.show', ['id' => Auth::user()->id]) }}"><i class="fa fa-btn fa-user"></i> Моя страница</a></li>
                            <li role="presentation" class="divider"></li>
                            <li><a href="{{ route('post.create') }}"><i class="fa fa-btn fa-plus-circle"></i> Сделать запись</a></li>
                            <li role="presentation" class="divider"></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Выйти</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

@yield('content')

        <!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"
        integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
<script src="/js/app.js"></script>
</body>
</html>
