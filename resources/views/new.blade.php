@extends('layouts.app')

@section('head')
    <title>Новые публикации</title>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <h3><i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i> Новые публикации</h3>
            <hr>
            <div class="row">
                @forelse($news as $post)
                    {{ Html::post($post) }}
                @empty
                    <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                @endforelse
            </div>
            <hr>
            {{$news->render()}}
        </div>
    </div>
@endsection
