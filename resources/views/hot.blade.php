@extends('layouts.app')

@section('head')
    <title>Самые обсуждаемые</title>
@endsection

@section('content')
    <div class="container">
        <div class="row m-post-group">
            <h3><i class="fa fa-comments fa-2x" aria-hidden="true"></i> Самые обсуждаемые в этом месяце</h3>
            <hr>
            <div class="row">
                @forelse($hots as $post)
                    {{ Html::post($post) }}
                @empty
                    <h3><i class="fa fa-meh-o" aria-hidden="true"></i> Список пуст</h3>
                @endforelse
            </div>
            <hr>
            {{$hots->render()}}
        </div>
    </div>
@endsection
