<?php

Route::auth();

//Home
Route::get('/', 'HomeController@index');
Route::get('/new', ['as' => 'home.new', 'uses' => 'HomeController@news']);
Route::get('/hot', ['as' => 'home.hot', 'uses' => 'HomeController@hot']);
Route::get('/search', ['as' => 'home.search', 'uses' => 'HomeController@search']);

//Post
Route::resource('post', 'PostController');

//Comment
Route::Post('/comment', ['as' => 'comment.store', 'uses' => 'CommentController@store']);
Route::Delete('/comment/{id}', ['as' => 'comment.destroy', 'uses' => 'CommentController@destroy']);

//User
Route::get('/feed', ['as' => 'user.feed', 'uses' => 'UserController@feed']);
Route::get('/{id}', ['as' => 'user.show', 'uses' => 'UserController@show']);
Route::get('/subscribe/{id}', ['as' => 'user.subscribe', 'uses' => 'UserController@subscribe']);
Route::get('/unsubscribe/{id}', ['as' => 'user.unsubscribe', 'uses' => 'UserController@unsubscribe']);

//File
Route::Post('/file/photo', 'FileController@photo');
Route::Post('/file/album', 'FileController@album');
Route::get('/file/album', 'FileController@allAlbum');
