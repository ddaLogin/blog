<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use Illuminate\Contracts\View\View;

class MainComposer
{
    /**
     * Привязка данных к представлению.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', Category::all());
    }
}
