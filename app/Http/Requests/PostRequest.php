<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     * @internal param \App\Http\Requests\Request $request
     */
    public function rules()
    {
        $id = Auth::user()->id;
        return [
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|between:6,50',
            'body' => 'required|min:10',
            'album_id' => "required|exists:albums,id,user_id,$id",
        ];
    }
}
