<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileRequest;
use App\Models\Album;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allAlbum()
    {
        return response()->json(Auth::user()->albums);
    }

    public function album(FileRequest $request)
    {
        $file = $request->file('album');
        $fileName = time().".".$file->getClientOriginalExtension();

        $move = $file->move("album",$fileName);

        $create = Auth::user()->albums()->save(new Album(['photo' => "/album/$fileName"]));

        if ($move && $create) return response("/album/$fileName", 200);
        else return response('', 500);
    }

    public function photo(FileRequest $request)
    {
        $user = Auth::user();
        $file = $request->file('photo');
        $fileName = $user->id.".".$file->getClientOriginalExtension();

        $move = $file->move("photos",$fileName);

        $update = $user->update(['photo' => "/photos/$fileName"]);

        if ($move && $update) return response("/photos/$fileName", 200);
        else return response('', 500);
    }
}
