<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = Post::news()->take(6)->get();
        $hots = Post::hots()->take(6)->get();
        return view('home', compact('news', 'hots'));
    }

    public function news()
    {
        $news = Post::news()->Paginate(15);
        return view('new', compact('news'));
    }

    public function hot()
    {
        $hots = Post::hots()->Paginate(15);
        return view('hot', compact('hots'));
    }

    public function search()
    {
        if (!isset($_GET["category_id"]) or empty($_GET["category_id"])){
            $posts = Post::where('title', 'like', "%$_GET[q]%")->Paginate(30);
        } else {
            $posts = Post::where('category_id', '=', $_GET["category_id"])->where('title', 'like', "%$_GET[q]%")->Paginate(30);
        }

        return view('search', compact('posts'));
    }
}
