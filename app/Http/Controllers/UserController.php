<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    public function show($id)
    {
        $user = User::findorfail($id);
        return view('user.show', compact('user'));
    }

    public function subscribe($id)
    {
        Auth::user()->subscriptions()->attach($id);
        return redirect()->back();
    }

    public function unsubscribe($id)
    {
        Auth::user()->subscriptions()->detach($id);
        return redirect()->back();
    }

    public function feed()
    {
        $posts = Post::whereIn('user_id', Auth::user()->subscriptions()->pluck('subscription'))->orderBy('posts.created_at', 'desc')->Paginate(15);
        return view('user.feed', compact('posts'));
    }
}
