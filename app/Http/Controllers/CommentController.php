<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('comment', ['only' => ['destroy']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        Auth::user()->comments()->save(new Comment($request->all()));
        return redirect()->route('post.show', ['id' => $request->input('post_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::destroy($id);
        return redirect()->back();
    }
}
