<?php

namespace App\Providers;

use Collective\Html\HtmlFacade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', 'App\Http\ViewComposers\MainComposer');

        HtmlFacade::component('post', 'components.post', ['post', 'size' => 'all']);
        HtmlFacade::component('comment', 'components.comment', ['comment']);
        HtmlFacade::component('user', 'components.user', ['user']);
        HtmlFacade::component('subscription', 'components.subscription', ['user']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
