<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $fillable = [
        'title', 'body', 'category_id', 'user_id', 'album_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function album()
    {
        return $this->belongsTo('App\Models\Album');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function getShortBody()
    {
        return mb_substr(strip_tags($this->body),0,150)."...";
    }

    public function scopeNews($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeHots($query)
    {
        return $query->select('posts.*')
            ->join('comments', 'comments.post_id', '=', 'posts.id')
            ->where(DB::raw('DATEDIFF(curdate(), posts.created_at)'), '<', '30')
            ->groupBy('comments.post_id')
            ->orderBy(DB::raw('COUNT(comments.post_id)'), 'desc');
    }
}
