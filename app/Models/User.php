<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function albums()
    {
        return $this->hasMany('App\Models\Album');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Models\User', 'subscriptions', 'subscription', 'subscriber');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('App\Models\User', 'subscriptions', 'subscriber', 'subscription');
    }
}
