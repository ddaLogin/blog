<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'photo', 'user_id'
    ];

    public function Posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}
