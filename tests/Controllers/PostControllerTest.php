<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * форма публикации
     */
    public function testCreateNotLogged()
    {
        $this->action('get', 'PostController@create');
        $this->assertRedirectedTo('/login');
    }

    public function testCreate()
    {
        $this->createAndLoginUser();

        $this->visit('/post/create')
            ->see('<title>Pencil| публикация</title>');

        $this->assertResponseOk();
    }

    public function testCreateForm()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);

        $this->visit('/post/create')
            ->select($category->id, 'category_id')
            ->type('Test title to new post', 'title')
            ->type($album->id, 'album_id')
            ->type('Test body to new post', 'body')
            ->press('Опубликовать');

        $this->SeeInDatabase('posts', ['user_id' => $user->id, 'album_id' => $album->id, 'category_id' => $category->id, 'title' => 'Test title to new post', 'body' => 'Test body to new post']);

        $this->assertResponseOk();

        $this->seePageIs("/post/".$user->posts[0]->id);
    }

    /**
     * сохранение
     */
    public function testStoreNotLogged()
    {
        $this->action('POST', 'PostController@store');
        $this->assertRedirectedTo('/login');
    }

    public function testStoreWithValidationError()
    {
        $this->createAndLoginUser();

        $response = $this->action('POST', 'PostController@store',['album_id' => 0, 'category_id' => 0, 'title' => '1', 'body' => '']);
        $this->dontSeeInDatabase('posts', ['album_id' => 0, 'category_id' => 0, 'title' => '1', 'body' => '']);
        $this->assertHasOldInput();
        $this->assertSessionHasErrors(['album_id', 'category_id', 'title', 'body']);
        $this->assertTrue($response->isRedirection());
    }

    public function testStore()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->action('POST', 'PostController@store',['album_id' => $album->id, 'category_id' => $category->id, 'title' => 'Test title to new post', 'body' => 'Test body to new post']);

        $this->SeeInDatabase('posts', ['album_id' => $album->id, 'category_id' => $category->id, 'title' => 'Test title to new post', 'body' => 'Test body to new post']);

        $this->assertTrue($response->isRedirection());
    }

    /**
     * Просмотр постов
     */
    public function testShowPostPage()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('GET', 'PostController@show', ['id' => $post->id]);

        $this->assertResponseOk();

        $this->assertViewHas('post');

        $postFromView = $response->original['post'];
        $this->assertEquals($post->id, $postFromView->id);
    }

    public function testShow404IfPostPageNotFound()
    {
        $response = $this->action('GET', 'PostController@show', ['id' => 0]);
        $this->assertEquals(404, $response->status());
    }

    /**
     * форма редактирования
     */
    public function testEditNotLogged()
    {
        $this->action('get', 'PostController@edit', ['id' => 0]);
        $this->assertRedirectedTo('/login');
    }

    public function testEditNotFoundPost()
    {
        $this->createAndLoginUser();

        $response = $this->action('get', 'PostController@edit', ['id' => 0]);

        $this->assertEquals(403, $response->status());
    }

    public function testEditNotYourPost()
    {
        $user = $this->createAndLoginUser();
        $anotherUser = factory(App\Models\User::class)->create();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $anotherUser->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $anotherUser->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('get', 'PostController@edit', ['id' => $post->id]);

        $this->assertEquals(403, $response->status());
    }

    public function testEditForm()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('GET', 'PostController@edit', ['id' => $post->id]);

        $this->assertResponseOk();

        $this->assertViewHas('post');

        $postFromView = $response->original['post'];
        $this->assertEquals($post->id, $postFromView->id);

        $this->seePageIs('/post/'.$post->id.'/edit');
        $this->see($post->title);
        $this->see($post->body);
        $this->see($post->category_id);
        $this->see($post->album->id);
        $this->see($post->album->photo);
    }

    /**
     * сохранение изминений
     */
    public function testUpdateNotLogged()
    {
        $this->action('Put', 'PostController@update', ['id' => 0]);
        $this->assertRedirectedTo('/login');
    }

    public function testUpdateNotYourPost()
    {
        $user = $this->createAndLoginUser();
        $anotherUser = factory(App\Models\User::class)->create();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $anotherUser->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $anotherUser->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('put', 'PostController@update', ['id' => $post->id]);

        $this->assertEquals(403, $response->status());
    }

    public function testUpdateNotFoundPost()
    {
        $this->createAndLoginUser();

        $response = $this->action('put', 'PostController@update', ['id' => 0]);

        $this->assertEquals(403, $response->status());
    }

    public function testUpdateWithValidationError()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('Put', 'PostController@update',['id' => $post->id, 'album_id' => 0, 'category_id' => 0, 'title' => '1', 'body' => '']);
        $this->dontSeeInDatabase('posts', ['id' => $post->id, 'album_id' => 0, 'category_id' => 0, 'title' => '1', 'body' => '']);
        $this->assertHasOldInput();
        $this->assertSessionHasErrors(['album_id', 'category_id', 'title', 'body']);
        $this->assertTrue($response->isRedirection());
    }

    public function testUpdate()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('Put', 'PostController@update',['id' => $post->id, 'album_id' => $album->id, 'category_id' => $category->id, 'title' => 'Test title to new post', 'body' => 'Test body to new post']);

        $this->SeeInDatabase('posts', ['id' => $post->id, 'album_id' => $album->id, 'category_id' => $category->id, 'title' => 'Test title to new post', 'body' => 'Test body to new post']);

        $this->assertRedirectedToRoute('post.show', ['id' => $post->id]);
    }

    /**
     * Удаление
     */
    public function testDestroyNotLogged()
    {
        $this->action('Delete', 'PostController@destroy',['id' => 0]);
        $this->assertRedirectedTo('/login');
    }

    public function testDestroyAlienPost()
    {
        $user = $this->createAndLoginUser();
        $anotherUser = factory(App\Models\User::class)->create();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $anotherUser->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $anotherUser->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('Delete', 'PostController@destroy',['id' => $post->id]);
        $this->assertEquals(403, $response->status());
    }

    public function testDestroy()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $response = $this->action('Delete', 'PostController@destroy',['id' => $post->id]);

        $this->dontSeeInDatabase('posts', ['id' => $post->id]);

        $this->assertRedirectedToRoute('user.show', ['id' => $user->id]);
    }

}
