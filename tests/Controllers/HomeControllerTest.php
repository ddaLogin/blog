<?php

class HomeControllerTest extends TestCase
{
    public function testIndexPage()
    {
        $this->visit('/')
            ->see('<title>Pencil</title>');
    }

    public function testNewsPage()
    {
        $this->visit('/new')
            ->see('<title>Новые публикации</title>');
    }

    public function testHotsPage()
    {
        $this->visit('/hot')
            ->see('<title>Самые обсуждаемые</title>');
    }
}
