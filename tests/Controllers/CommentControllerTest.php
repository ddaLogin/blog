<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * сохранение
     */
    public function testStoreNotLogged()
    {
        $this->action('POST', 'CommentController@store');
        $this->assertRedirectedTo('/login');
    }

    public function testStoreWithValidationError()
    {
        $this->createAndLoginUser();

        $response = $this->action('POST', 'CommentController@store',['post_id' => 0, 'body' => '1']);
        $this->assertHasOldInput();
        $this->assertSessionHasErrors(['post_id', 'body']);
        $this->assertTrue($response->isRedirection());
    }

    public function testStore()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);

        $this->action('POST', 'CommentController@store',['post_id' => $post->id, 'body' => 'comment body']);

        $this->SeeInDatabase('comments', ['post_id' => $post->id, 'user_id' => $user->id, 'body' => 'comment body']);

        $this->assertRedirectedToRoute('post.show', ['id' => $post->id]);
    }

    /**
     * Удаление
     */
    public function testDestroyNotLogged()
    {
        $this->action('Delete', 'CommentController@destroy',['id' => 0]);
        $this->assertRedirectedTo('/login');
    }

    public function testDestroyAlienComment()
    {
        $user = $this->createAndLoginUser();
        $anotherUser = factory(App\Models\User::class)->create();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $anotherUser->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $anotherUser->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);
        $comment = factory(App\Models\Comment::class)->create([
            'user_id' => $anotherUser->id,
            'post_id' => $post->id,
        ]);

        $response = $this->action('Delete', 'CommentController@destroy',['id' => $comment->id]);
        $this->assertEquals(403, $response->status());
    }

    public function testDestroy()
    {
        $user = $this->createAndLoginUser();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $user->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $user->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);
        $comment = factory(App\Models\Comment::class)->create([
            'user_id' => $user->id,
            'post_id' => $post->id,
        ]);

        $this->SeeInDatabase('comments', ['post_id' => $post->id, 'user_id' => $user->id, 'body' => $comment->body]);

        $response = $this->action('Delete', 'CommentController@destroy',['id' => $comment->id]);

        $this->dontSeeInDatabase('comments', ['post_id' => $post->id, 'user_id' => $user->id, 'body' => $comment->body]);

        $this->assertTrue($response->isRedirection());
    }
}
