<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Страницы пользователей
     */
    public function testShowUserPage()
    {
        $user = factory(App\Models\User::class)->create();

        $response = $this->action('GET', 'UserController@show', ['id' => $user->id]);

        $this->assertResponseOk();

        $this->assertViewHas('user');

        $userFromView = $response->original['user'];
        $this->assertEquals($user->id, $userFromView->id);
    }

    public function testShow404IfUserPageNotFound()
    {
        $response = $this->action('GET', 'UserController@show', ['id' => 0]);
        $this->assertEquals(404, $response->status());
    }


    /**
     * Подписки
     */
    public function testSubscribeNotLogged()
    {
        $this->action('GET', 'UserController@subscribe', ['id' => 1]);
        $this->assertRedirectedTo('/login');
    }

    public function testSubscribeOnNotCreatedUser()
    {
        $this->createAndLoginUser();
        $response = $this->action('GET', 'UserController@subscribe', ['id' => 0]);
        $this->assertEquals(500, $response->status());
    }

    public function testSubscribeAndBackRedirect()
    {
        $user = $this->createAndLoginUser();
        $userToSubscribe = factory(App\Models\User::class)->create();

        $response = $this->action('GET', 'UserController@subscribe', ['id' => $userToSubscribe->id]);

        $this->seeInDatabase('subscriptions', ['subscriber' => $user->id, 'subscription' => $userToSubscribe->id]);

        $this->assertTrue($response->isRedirection());
    }

    /**
     * Отписываемся
     */
    public function testUnsubscribeNotLogged()
    {
        $this->action('GET', 'UserController@unsubscribe', ['id' => 1]);
        $this->assertRedirectedTo('/login');
    }

    public function testUnsubscribeFromUserOnNotSigned()
    {
        $this->createAndLoginUser();
        $response = $this->action('GET', 'UserController@unsubscribe', ['id' => 0]);
        $this->assertTrue($response->isRedirection());
    }

    public function testUnsubscribeAndBackRedirect()
    {
        $userToSubscribe = factory(App\Models\User::class)->create();
        $user = $this->createAndLoginUser();
        $user->subscriptions()->attach($userToSubscribe->id);

        $this->SeeInDatabase('subscriptions', ['subscriber' => $user->id, 'subscription' => $userToSubscribe->id]);

        $response = $this->action('GET', 'UserController@unsubscribe', ['id' => $userToSubscribe->id]);

        $this->dontSeeInDatabase('subscriptions', ['subscriber' => $user->id, 'subscription' => $userToSubscribe->id]);

        $this->assertTrue($response->isRedirection());
    }
    
    /**
     * Лента
     */
    public function testFeedNotLogged()
    {
        $this->action('GET', 'UserController@feed');
        $this->assertRedirectedTo('/login');
    }

    public function testFeed()
    {
        $userToSubscribe = factory(App\Models\User::class)->create();
        $category = factory(App\Models\Category::class)->create();
        $album = factory(App\Models\Album::class)->create([
            'user_id' => $userToSubscribe->id
        ]);
        $post = factory(App\Models\Post::class)->create([
            'user_id' => $userToSubscribe->id,
            'category_id' => $category->id,
            'album_id' => $album->id
        ]);
        $user = $this->createAndLoginUser();
        $user->subscriptions()->attach($userToSubscribe->id);

        $this->SeeInDatabase('subscriptions', ['subscriber' => $user->id, 'subscription' => $userToSubscribe->id]);

        $response = $this->action('GET', 'UserController@feed');

        $this->assertResponseOk();

        $this->assertViewHas('posts');

        $feedPosts = $response->original['posts'];
        $this->assertEquals(1, count($feedPosts));
        $this->assertEquals($post->id, $feedPosts[0]->id);
    }
}
