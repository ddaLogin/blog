<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->text
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('r671r329'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Album::class, function (Faker\Generator $faker) {
    return [
        'user_id' => 1,
        'photo' => $faker->text(10)
    ];
});

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'category_id' => 1,
        'title' => $faker->title,
        'body' => $faker->text,
        'user_id' => 1,
        'album_id' => 1,
    ];
});

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'post_id' => 1,
        'body' => $faker->text,
        'user_id' => 1,
    ];
});

