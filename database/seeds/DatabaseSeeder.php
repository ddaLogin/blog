<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SubscriptionSeeder::class);
        $this->call(AlbumSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(PostSeeder::class);
        $this->call(CommentSeeder::class);
    }
}

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        \App\Models\User::create([
            'name' => "Максим Сапожная",
            'email' => "sapojnaya@gmail.com",
            'password' => bcrypt("r671r329"),
            'photo' => "/photos/1.jpg",
        ]);

        \App\Models\User::create([
            'name' => "Васильев Николай",
            'email' => "kolka@gmail.com",
            'password' => bcrypt("r671r329"),
            'photo' => "/photos/2.jpg",
        ]);

        \App\Models\User::create([
            'name' => "Jon Smith",
            'email' => "jon@gmail.com",
            'password' => bcrypt("r671r329"),
            'photo' => "/photos/3.jpg",
        ]);

        \App\Models\User::create([
            'name' => "Евдакимова Алина",
            'email' => "alina@gmail.com",
            'password' => bcrypt("r671r329"),
            'photo' => "/photos/4.jpg",
        ]);

        \App\Models\User::create([
            'name' => "Сидоров Олег",
            'email' => "oleg@gmail.com",
            'password' => bcrypt("r671r329"),
            'photo' => "/photos/5.jpg",
        ]);
    }
}

class SubscriptionSeeder extends Seeder
{
    public function run()
    {
        DB::table('subscriptions')->delete();

        DB::table('subscriptions')->insert(['subscriber' => '1', 'subscription' => '2']);
        DB::table('subscriptions')->insert(['subscriber' => '1', 'subscription' => '3']);
        DB::table('subscriptions')->insert(['subscriber' => '1', 'subscription' => '4']);
        DB::table('subscriptions')->insert(['subscriber' => '1', 'subscription' => '5']);

        DB::table('subscriptions')->insert(['subscriber' => '2', 'subscription' => '1']);
        DB::table('subscriptions')->insert(['subscriber' => '2', 'subscription' => '3']);
        DB::table('subscriptions')->insert(['subscriber' => '2', 'subscription' => '4']);
        DB::table('subscriptions')->insert(['subscriber' => '2', 'subscription' => '5']);

        DB::table('subscriptions')->insert(['subscriber' => '3', 'subscription' => '1']);
        DB::table('subscriptions')->insert(['subscriber' => '3', 'subscription' => '2']);
        DB::table('subscriptions')->insert(['subscriber' => '3', 'subscription' => '5']);

        DB::table('subscriptions')->insert(['subscriber' => '4', 'subscription' => '1']);
        DB::table('subscriptions')->insert(['subscriber' => '4', 'subscription' => '5']);
    }
}

class AlbumSeeder extends Seeder
{
    public function run()
    {
        DB::table('albums')->delete();

        \App\Models\Album::create(['photo' => "/album/1_1.jpg", 'user_id' => "1"]);

        \App\Models\Album::create(['photo' => "/album/3_1.jpg", 'user_id' => "3"]);

        \App\Models\Album::create(['photo' => "/album/4_1.jpg", 'user_id' => "4"]);
    }
}

class CategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->delete();

        \App\Models\Category::create(['name' => "Музыка"]);
        \App\Models\Category::create(['name' => "Спорт"]);
        \App\Models\Category::create(['name' => "Технологии"]);
        \App\Models\Category::create(['name' => "Прочее"]);
    }

}

class PostSeeder extends Seeder
{
    public function run()
    {
        DB::table('posts')->delete();

        \App\Models\Post::create([
            'category_id' => "1",
            'album_id' => "1",
            'user_id' => "1",
            'title' => "История формирования группы Muse",
            'body' => "
            <div><b>Формирование и ранние годы (1992—1997)</b></div><div>В начале девяностых годов Мэттью, Доминик и Крис играли в разных коллективах в Тинмутской школе. Формирование Muse началось после того, как Мэттью Беллами стал гитаристом в группе Доминика Ховарда. Они попали в трудную ситуацию, когда их второй гитарист Phys Vandit решил уйти. Тогда они попросили своего близкого друга Криса Уолстенхолма научиться играть на бас-гитаре. С тех пор группа сменила большое количество названий, таких как: Gothic Plague, Carnage Mayhem, Fixed Penalty и Rocket Baby Dolls, прежде чем стать Muse (перечисление абстрактное, сведения в разных интервью отличаются друг от друга).</div><div><br></div><div>В 1994 году, под названием Rocket Baby Dolls, Muse выиграли местную «Битву групп», разрушив в процессе выступления своё оборудование. (Они были «единственной настоящей рок-группой».) Вскоре после этого они решили не поступать в университет, а погрузиться в музыкальную среду. Они сменили название на Muse и начали выступать в местных клубах вроде «The Cavern» в Эксетере.</div>
            "
        ]);

        \App\Models\Post::create([
            'category_id' => "4",
            'album_id' => "2",
            'user_id' => "3",
            'title' => "Что такое Green card (Грин-карта)",
            'body' => "<b style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">Грин-карта&nbsp;</b><span style=\"line-height: 21.4286px;\">-</span><b style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;</b>удостоверение личности или так называемая идентификационная карта, подтверждающая наличие вида на жительство у человека, не являющегося гражданином <a href=\"http://https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D0%B5%D0%B4%D0%B8%D0%BD%D1%91%D0%BD%D0%BD%D1%8B%D0%B5_%D0%A8%D1%82%D0%B0%D1%82%D1%8B_%D0%90%D0%BC%D0%B5%D1%80%D0%B8%D0%BA%D0%B8\" title=\"США\" target=\"_blank\">США</a>, но постоянно проживающего на территории США, и предоставляющая право трудоустройства на территории этой страны.&nbsp;<div><br></div><div>Зелёными «грин-карты» были в период с 1946 по 1964 года и вернулись к этой цветовой гамме после очередного обновления дизайна 11 мая 2010 года. Теперь Green Card (дословный перевод — «зелёная карта») оправдывает своё название. Зелёная карта выдаётся после достаточно долгого и дорогостоящего процесса сбора документов и подачи необходимых заявлений.<br></div><div><br></div><div><img src=\"http://frenchmorning.com/wp-content/uploads/2016/05/greencard-101b6567.jpg\" alt=\"Грин карта\" align=\"left\"><br></div>"
        ]);

        \App\Models\Post::create([
            'category_id' => "4",
            'album_id' => "3",
            'user_id' => "4",
            'title' => "Lorem Ipsum",
            'body' => "<b style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">Lorem ipsum</b><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;— классическая&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D0%BD%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0\" title=\"Панграмма\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">панграмма</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">, условный, зачастую бессмысленный&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/%D0%A2%D0%B5%D0%BA%D1%81%D1%82\" title=\"Текст\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">текст</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">-заполнитель, вставляемый в</span><a href=\"https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D0%BA%D0%B5%D1%82\" title=\"Макет\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">макет</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;страницы. Является искажённым отрывком из философского&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/%D0%A2%D1%80%D0%B0%D0%BA%D1%82%D0%B0%D1%82_(%D0%BB%D0%B8%D1%82%D0%B5%D1%80%D0%B0%D1%82%D1%83%D1%80%D0%B0)\" title=\"Трактат (литература)\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">трактата</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%80%D0%BA_%D0%A2%D1%83%D0%BB%D0%BB%D0%B8%D0%B9_%D0%A6%D0%B8%D1%86%D0%B5%D1%80%D0%BE%D0%BD\" title=\"Марк Туллий Цицерон\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">Марка Туллия Цицерона</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;«О пределах добра и зла», написанного в&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/45_%D0%B3%D0%BE%D0%B4_%D0%B4%D0%BE_%D0%BD._%D1%8D.\" title=\"45 год до н. э.\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">45 году до&nbsp;н.&nbsp;э.</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">&nbsp;на&nbsp;</span><a href=\"https://ru.wikipedia.org/wiki/%D0%9B%D0%B0%D1%82%D0%B8%D0%BD%D1%81%D0%BA%D0%B8%D0%B9_%D1%8F%D0%B7%D1%8B%D0%BA\" title=\"Латинский язык\" style=\"color: rgb(11, 0, 128); font-family: sans-serif; font-size: 14px; line-height: 22.4px; background: none rgb(255, 255, 255);\">латинском языке</a><span style=\"color: rgb(37, 37, 37); font-family: sans-serif; font-size: 14px; line-height: 22.4px;\">.</span><br>"
        ]);
    }
}

class CommentSeeder extends Seeder
{
    public function run()
    {
        DB::table('comments')->delete();

        \App\Models\Comment::create(['user_id' => "4", 'post_id' => "1", 'body' => "Muse моя любимая группа",]);
        \App\Models\Comment::create(['user_id' => "2", 'post_id' => "1", 'body' => "Как зовут главного вокалиста?",]);

        \App\Models\Comment::create(['user_id' => "1", 'post_id' => "2", 'body' => "А не могли бы вы расскзать о лотерее в которой разыгрываются грин карты?",]);
        \App\Models\Comment::create(['user_id' => "4", 'post_id' => "2", 'body' => "Да, я бы тоже хотела узнать где и как эта лотерея проходит, в интернете на эту тему много мошейников водится",]);
        \App\Models\Comment::create(['user_id' => "3", 'post_id' => "2", 'body' => "Хорошо на днях добавлю к этому посту информацию о лотерее",]);
    }

}
