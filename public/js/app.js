var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

$(document.body).on('click', '*[data-bind-value]' ,function(){
    var value = $(this).data("bind-value");
    var target = $($(this).data("bind-target"));
    target.val(value);
});

$(document.body).on('click', '*[data-bind-mirror]' ,function(){
    var text = $(this).html();
    var target = $($(this).data("bind-mirror"));
    target.html(text);
});

$(document.body).on('click', '*[data-bind-src]' ,function(){
    var target = $($(this).data("bind-src"));
    $(target).src = $(this).src;
});

$("*[data-ajax='upload']").change(function () {
    var name = $(this).attr("name");
    var progress = $($(this).data("ajax-progress"));
    $(progress).toggleClass('hidden', '');
    var complete = $(this).data("ajax-complete");
    var data = new FormData();

    data.append('_token', CSRF_TOKEN);
    $.each(this.files, function (key, value) {
        data.append(name, value);
    });
    $.ajax({
        xhr: function()
        {
            var xhr = new window.XMLHttpRequest();
            // прогресс загрузки на сервер
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    $($(progress).children(0)).width(percentComplete*100+"%");
                }
            }, false);
            return xhr;
        },
        url: $(this).data("upload-link"),
        type: 'POST',
        data: data,
        cache: false,
        dataType: false,
        processData: false,
        contentType: false,
        success: function (respond, textStatus, jqXHR) {
            window[complete](respond);
        }
    });
});

function updateAlbums(respond) {
    $.ajax({
        type: "GET",
        url: "/file/album",
        data: '',
        success: function (data) {
            $("#albumList").html('');
            $.each(data, function (index, item) {
                var content = $('<div onclick="setPreview(&quot;'+item.id+'&quot;, &quot;'+item.photo+'&quot;)" class="m-album col col-md-3" style="background-image: url('+item.photo+');" data-dismiss="modal"></div>');
                $("#albumList").append(content);
            });
            $("#uploadProgress").toggleClass('hidden');
            $("#uploadProgress").children(0).width("0%");
        },
        dataType: "JSON"
    });
}

function updateAvatar(respond) {
    $("#photo").attr('src', respond + "?" + new Date().getTime());
    $("#uploadProgress").toggleClass('hidden');
    $("#uploadProgress").children(0).width("0%");
}

function setPreview(id, src) {
    $("#albumId").val(id);
    $("#albumPreview").attr('src', src);
}

$("*[data-click]").click(function () {
    $($(this).data('click')).click();
});
